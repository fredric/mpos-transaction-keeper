The app scrapes the HTML pages since the API only exports the last 30 transactions.
The HTML pages show several weeks of transactions - you only need to run this app
once a week to keep a full record of all your transactions.
If the API had been used this app would have to run every minute and never fail on
either side, to keep a full record of your transactions.

Transactions are stored in a sqlite database.

You can scrape one pool per run, but you can store transactions from all pools
by running it once per pool you are mining on. You get earning per day from all pools.

Example fo fetching transactions from two pools into one db:

    node index.js fetch -f data.db -l "https://zec.suprnova.cc/index.php?page=account&action=transactions" -n "zec.suprnova.com" -s "asljdhfq9834fh038h4fhq3048h"

    node index.js fetch -f data.db -l "https://zen.suprnova.cc/index.php?page=account&action=transactions" -n "zen.suprnova.com" -s "kl4s4gdWj4GfkDl4sdjf892df34"

Run the command again once per week or so to get all transactions, only the new ones will be fetched.

If something bails and you miss transactions, use the '-f' option to fetch old transactions as well. The missing ones will be inserted.
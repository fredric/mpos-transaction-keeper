const XLSX = require('xlsx');

function writeWorkbook(data, filename) {
  const workbook = XLSX.utils.book_new();
  const worksheet = XLSX.utils.json_to_sheet(
    data,
    { cellDates: true },
  );
  XLSX.utils.book_append_sheet(workbook, worksheet, 'Summary');
  XLSX.writeFile(workbook, filename);
}

exports.writeWorkbook = writeWorkbook;

const cloudscraper = require('cloudscraper');
const { JSDOM } = require('jsdom');
const sqlite3 = require('sqlite3').verbose();
const Bluebird = require('bluebird');
const program = require('commander');
const prompt = require('prompt');
const moment = require('moment-timezone');
const { writeWorkbook } = require('./src/save-xlsx');

const DEFAULT_DELAY_BETWEEN_PAGE_REQUESTS = 2000;
const OFFSET_REPLACE_KEY = 'OFFSET_REPLACE_KEY';

function fetchPage(offset, pool, poolOptions) {
  return new Bluebird((resolve) => {
    console.log('Fetching page...');
    cloudscraper.get(
      pool.transactionsPage.replace(OFFSET_REPLACE_KEY, offset),
      (err, response, body) => {
        if (err) {
          throw err.error;
        }
        if (body.indexOf('Request rate limit exceeded') > -1) {
          throw new Error('Request rate limit exceeded');
        }
        console.log('done.');
        resolve(body);
      },
      {
        Cookie: `PHPSESSID=${poolOptions.phpsessid}`,
      },
    );
  });
}

const transformNumber = t => Number(t.replace(/[+ -]/g, ''));
const transformDateString = cell => t => moment.tz(t, cell.dateFormat, cell.timeZone).unix();
const transformString = t => t.trim();
const transformBool = t => !!t.trim();

function getTransform(cell) {
  switch (cell.type) {
  case 'number':
    return transformNumber;

  case 'datestring':
    return transformDateString(cell);

  case 'string':
    return transformString;

  case 'bool':
    return transformBool;

  default:
    return null;
  }
}

function getCellTransforms(pool) {
  return pool.cells.map(cell => cell.map(cellToDb => ({
    dbColumn: cellToDb.dbColumn,
    transform: getTransform(cellToDb),
  })));
}

function mapHtmlTransactionToObject(htmlTransaction, pool, cellsTransforms) {
  const transaction = {
    pool: pool.name,
    currency: pool.currency,
    txType: 'Credit',
    confirmed: 'Confirmed',
  };
  htmlTransaction
    .querySelectorAll(pool.transactionsCellSelector)
    .forEach((htmlCell, i) => {
      const cellTransforms = cellsTransforms[i];
      cellTransforms.forEach((ct) => {
        transaction[ct.dbColumn] = ct.transform(htmlCell.textContent);
      });
    });
  return transaction;
}

function getTransactionsFromBody(body, pool) {
  const dom = new JSDOM(body);
  const { document } = dom.window;
  const transactions = [];
  const cellTransforms = getCellTransforms(pool);
  const selector = pool.transactionsRowSelector;
  document.querySelectorAll(selector).forEach((t) => {
    transactions.push(mapHtmlTransactionToObject(t, pool, cellTransforms));
  });
  console.log(`Found ${transactions.length} transactions in body.`);
  return transactions;
}

function takeNewConfirmedTransactions(transactions, lastTransaction, configOptions) {
  const confirmedTransactions = transactions.filter(t => t.confirmed);
  const confirmedNewTransactions = confirmedTransactions.filter(t => configOptions.force || t.id > lastTransaction.id);
  const olderTransactionFound = !!transactions.find(t => t.id < lastTransaction.id);
  if (!configOptions.force && olderTransactionFound) {
    console.log('Older transaction found. Not forcing fetch of older transactions. Nothing more to fetch.');
  } else {
    console.log(`Took ${confirmedNewTransactions.length} confirmed new transactions`);
  }
  return {
    confirmedNewTransactions,
    transactionsOnPage: transactions.length,
    doContinue:
      transactions.length &&
      (configOptions.force || !olderTransactionFound),
  };
}

function getLastTransactionFromDb(db, options) {
  return new Promise((resolve) => {
    const query = `
      SELECT *
      FROM \`transactions\`
      WHERE pool='${options.pool.name}'
      ORDER BY id DESC
      LIMIT 1
    `;
    db.get(query, (err, row) => {
      if (row) {
        resolve(row);
      }
      resolve({ id: 0 });
    });
  });
}

function createTable(db) {
  db.serialize(() => {
    db.run(`
      CREATE TABLE IF NOT EXISTS transactions (
        pool TEXT NOT NULL,
        currency TEXT NOT NULL,
        id TEXT NOT NULL,
        timestamp INTEGER NOT NULL,
        txType TEXT,
        paymentAddress TEXT,
        transactionNumber TEXT,
        blockNumber INTEGER,
        amount STRING NOT NULL,
        PRIMARY KEY(pool, id)
      )
    `);
  });
}

function saveTransactionToDb(stmt, t) {
  return new Promise((resolve) => {
    stmt.run(
      t.pool, t.id, t.timestamp, t.txType, t.paymentAddress, t.transactionNumber, t.blockNumber, t.amount,
      (err) => {
        if (err) {
          throw err;
        }
        resolve();
      },
    );
  });
}

async function saveTransactionsToDb(transactions, db) {
  const stmt = db.prepare('INSERT OR IGNORE INTO transactions VALUES (?, ?, ?, ?, ?, ?, ?, ?)');
  await Bluebird.resolve(transactions)
    .mapSeries(t => saveTransactionToDb(stmt, t));
  stmt.finalize();
  return transactions.length;
}

function openDb(filename) {
  return new Promise((resolve) => {
    const db = new sqlite3.Database(filename, (err) => {
      if (err) {
        throw err;
      }
      resolve(db);
    });
  });
}

function run(db, startTransaction, configOptions, pool, poolOptions, offset = 0) {
  let currentDb = db;
  console.log('Run with offset: ', offset);
  let transactionsOnPage = 0;
  let doContinue = false;
  let savedTransactions = 0;
  return fetchPage(offset, pool, poolOptions)
    .then(body => getTransactionsFromBody(body, pool))
    .then(transactions => takeNewConfirmedTransactions(transactions, startTransaction, configOptions, pool))
    .then((res) => {
      /* eslint-disable prefer-destructuring */
      transactionsOnPage = res.transactionsOnPage;
      savedTransactions = res.confirmedNewTransactions;
      doContinue = res.doContinue;
      /* eslint-enable prefer-destructuring */
      return saveTransactionsToDb(res.confirmedNewTransactions, currentDb, pool);
    })
    .then(() => {
      if (doContinue) {
        console.log(`Delay ${DEFAULT_DELAY_BETWEEN_PAGE_REQUESTS} ms.`);
        return Bluebird.delay(DEFAULT_DELAY_BETWEEN_PAGE_REQUESTS)
          .then(() => console.log('Delay done.'))
          .then(() => run(currentDb, startTransaction, configOptions, pool, poolOptions, offset + transactionsOnPage));
      }
      return savedTransactions;
    })
    .catch(async (e) => {
      console.log('Caught error', e);
      if (e.code === 'ENOTFOUND') {
        console.log('Network error. Retrying.');
        return run(currentDb, startTransaction, configOptions, pool, poolOptions, offset);
      }
      if (e.message === 'SQLITE_MISUSE') {
        console.log('Database was closed. Try to open it again.');
        currentDb = await openDb(configOptions.database);
        // Open db, run again with current offset
        return run(currentDb, startTransaction, configOptions, pool, poolOptions, offset);
      }
      if (e.message === 'Request rate limit exceeded') {
        console.log(`Request rate limit exceeded. Backing off. Wating ${DEFAULT_DELAY_BETWEEN_PAGE_REQUESTS * 3} ms.`);
        return Bluebird.delay(DEFAULT_DELAY_BETWEEN_PAGE_REQUESTS * 3)
          .then(() => run(currentDb, startTransaction, configOptions, pool, poolOptions, offset));
      }
      return null;
    });
}

function readPoolOptions(pool) {
  console.log(`Input details for pool ${pool.name}`);
  return new Promise((resolve) => {
    prompt.get(['phpsessid', 'offset'], (err, result) => {
      const poolDetails = {
        offset: Number(result.offset),
        phpsessid: result.phpsessid,
      };
      resolve(poolDetails);
    });
  });
}

// TODO read json by reading file and parsing it, don't use require
const config = require('./config.json');

// Start
async function fetch(options) {
  moment.tz.setDefault();
  // const config = require(options.config);
  const db = await openDb(config.database);
  const configOptions = { ...config, ...options };
  try {
    createTable(db);
    await Bluebird.resolve(config.pools)
      .mapSeries(async (pool) => {
        const poolOptions = await readPoolOptions(pool);
        const startTransaction = await getLastTransactionFromDb(db, { pool });
        console.log(`Starting scrape of pool: ${pool.name}`);
        await run(db, startTransaction, configOptions, pool, poolOptions, poolOptions.offset);
      });
    console.log('All done.');
  } catch (e) {
    console.error('An error has occured:', e);
  }
  db.close();
}

async function getDataForPeriod() {
  const db = await openDb(config.database);
  const data = new Map();
  const columns = new Set();
  return new Promise((resolve) => {
    const query = `
      SELECT
        currency,
        txType,
        SUM(amount) as sumamount,
        date(timestamp / 1000, 'unixepoch') as datefield
      FROM transactions
      GROUP BY currency, txType, datefield
      ORDER BY datefield DESC
    `;
    db.serialize(() => {
      db.each(query, (err, row) => {
        console.log('each');
        if (err) {
          throw err;
        }
        const currentDateData = data.get(row.datefield) || {};
        const column = `${row.currency}-${row.txType}`;
        data.set(row.datefield, {
          ...currentDateData,
          [column]: row.sumamount,
        });
        columns.add(column);
      });
    });
    resolve({
      columns,
      data,
    });
  });
  /*
  return [
    {
      date: '2018-03-01',
      'XMR-TOTAL': 0.01,
      'XMR-FEE': 0.001,
      XMR: 0.009,
      'BTCP-TOTAL': 0,
      'BTCP-FEE': 0,
      BTCP: 0,
      'ZEN-TOTAL': 0,
      'ZEN-FEE': 0,
      ZEN: 0,
      'ZEC-TOTAL': 0,
      'ZEC-FEE': 0,
      ZEC: 0,
      'ETH-TOTAL': 0.0012,
      'ETH-FEE': 0.0000115,
      ETH: 0.0011885,
    },
    {
      date: '2018-03-02',
      'XMR-TOTAL': 0.1,
      'XMR-FEE': 0.01,
      XMR: 0.09,
      'BTCP-TOTAL': 0,
      'BTCP-FEE': 0,
      BTCP: 0,
      'ZEN-TOTAL': 0,
      'ZEN-FEE': 0,
      ZEN: 0,
      'ZEC-TOTAL': 0,
      'ZEC-FEE': 0,
      ZEC: 0,
      'ETH-TOTAL': 0.012,
      'ETH-FEE': 0.000115,
      ETH: 0.011885,
    },
  ];
  */
}

program
  .command('fetch')
  .description('fetch transactions from all pools, store in database')
  .option('-c, --config <config.json>', 'json config file to read options from', 'config.json')
  .option('-f, --force <false>', 'Should we try to fetch transactions older than the latest one in db?', false)
  .action((options) => {
    fetch(options);
  });

program
  .command('spreadsheet')
  .description('calculate income per day, per currency, and write them to a spreadsheet')
  .option('-f, --filename <filename>', 'Filename of output spreadsheet.', 'data.xlsb')
  .action((options) => {
    getDataForPeriod(
      moment('2018-01-01 00:00:00', 'Europe/Stockholm'),
      moment('2018-04-31 00:00:00', 'Europe/Stockholm'),
    ).then(wsData => {
      console.log(wsData.columns);
      console.log(wsData.data);
      writeWorkbook(wsData, options.filename);
    });
  });

if (!process.argv.slice(2).length) {
  program.outputHelp();
}

program.parse(process.argv);
